/**
 * Holds static config as of now
 * Created by prakhar on 28/08/16.
 */

module.exports = {
  port: 4000,
  ip: '0.0.0.0'
};