var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
});

/* GET other pages. */
router.get('/:page', function (req, res, next) {
  var page = req.params.page;
  if (page.indexOf('.') > -1) res.render('error/404');
  else {
    res.render(page, {}, function (err, html) {
      if (err) {
        res.render('error/404');
      } else {
        res.send(html);
      }
    });
  }
});

module.exports = router;
